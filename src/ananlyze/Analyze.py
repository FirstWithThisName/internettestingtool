import matplotlib.pyplot as plt
import csv
import datetime
from tkinter import filedialog as fd

# get stored file
STORINFILE = fd.askopenfilename()

times = []
uploads = []
downloads = []
pings = []

# fetch data
with open(STORINFILE, 'r') as csvfile:
    spamreader = csv.reader(csvfile)
    for row in spamreader:
        times.append(row[0])
        pings.append(row[1])
        downloads.append(row[2])
        uploads.append(row[3])


plt.plot(downloads, label="download speed")
plt.plot(uploads, label="upload speed")
plt.legend()
plt.grid()
plt.title("internet speed")
plt.ylabel("Mbps")
dates = []

for time in times:
    dates.append(datetime.datetime.fromtimestamp(
        int("1284105682")
    ).strftime('%Y-%m-%d %H:%M:%S'))
#plt.xticks(dates)
plt.show()
