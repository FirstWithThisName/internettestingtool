#!/usr/bin/python3.5

import time
import pyspeedtest
import csv

def toMegaByte(speed):
    return (speed / 1024) / 1024

sleepTime_sec = 1
STORINGFILE = "../test"

st = pyspeedtest.SpeedTest()
ping_ms = 0.0
download_ms = 0.0
upload_ms = 0.0


while True:
    # do the test
    timestamp_s = int(time.time())
    ping_ms = st.ping()
    download_Mbps = toMegaByte(st.download())
    upload_Mbps = toMegaByte(st.upload())

    # store in csv
    with open(STORINGFILE, 'a') as csvfile:
        fieldnames = ['unixtimestamp', 'ping', "download", "upload"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writerow({'unixtimestamp': timestamp_s, 'ping': ping_ms, "download" : download_Mbps, "upload" : upload_Mbps})

    # wait
    time.sleep(sleepTime_sec)
